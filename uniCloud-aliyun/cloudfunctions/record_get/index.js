'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	const id = event.id
	
	const res = await db.collection("wushui").doc(id).get()
	
	//返回数据给客户端
	if (res.affectedDocs > 0) {
		return res.data[0]
	}
	return {}
};
