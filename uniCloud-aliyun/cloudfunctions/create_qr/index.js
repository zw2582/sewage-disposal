'use strict';

const openapi = require('mp-cloud-openapi')
const openapiWeixin = openapi.initWeixin({
  appId: 'wx503d9fb77d73c8fe',
  secret: 'dfc5701bb8cd8e122a61080d920bfe52'
})

exports.main = async (event, context) => {
	try {
		const authResult = await openapiWeixin.auth.getAccessToken()
	    const result = await openapiWeixin.wxacode.getUnlimited({
	        page: event.path,
			scene:event.id,
	        width: 430,
			accessToken:authResult.accessToken
	      })

		console.log(event, result)
		// const base64 = Buffer.from(result.buffer).toString('base64');
		// console.log(base64)

	    return result
	  } catch (err) {
	    return err
	  }
};
