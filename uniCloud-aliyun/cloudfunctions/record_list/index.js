'use strict';

const db = uniCloud.database()
exports.main = async (event, context) => {
	const res = await db.collection("wushui").orderBy("updated_at", "desc").get()
	
	return res.data
};
