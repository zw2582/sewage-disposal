'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	let id = event.id
	const data = event.data
	data.updated_at = Date.parse(new Date())
	if (id) {
		delete(data._id)
		await db.collection("wushui").doc(id).update(data)
	} else {
		const res = await db.collection("wushui").add(data)
		id = res.id
	}
	return id
};
